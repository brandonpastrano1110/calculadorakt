package com.example.practicak01

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {
    private lateinit var txtUsuario : TextView
    private lateinit var txtNum1 : EditText
    private lateinit var txtNum2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMulti: Button
    private lateinit var btnDiv: Button

    private lateinit var btnLimpiar : Button
    private lateinit var btnSalir : Button

    private lateinit var operaciones: Operaciones

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventosClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    fun iniciarComponentes(){
        txtUsuario = findViewById(R.id.txtUsuario)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        txtResultado = findViewById(R.id.txtResultado)
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar= findViewById(R.id.btnRestar)
        btnMulti = findViewById(R.id.btnMulti)
        btnDiv = findViewById(R.id.btnDiv)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnSalir = findViewById(R.id.btnSalir)

        val bundle : Bundle? = intent.extras
        if (bundle != null) {
            txtUsuario.text = bundle.getString("nombre", "Desconocido")
        } else {
            txtUsuario.text = "Usuario: Desconocido"
        }
    }
    var num1 = 0.0f
    var num2 = 0.0f

    fun isValido() : Boolean{
        if(txtNum1.text.toString().contentEquals("")||
            txtNum2.text.toString().contentEquals("")){
            Toast.makeText(this, "Llena los dos numeros", Toast.LENGTH_SHORT).show()
            return false
        }
        else{
            num1 = convertirFloat(txtNum1.text.toString())
            num2 = convertirFloat(txtNum2.text.toString())
            return true
        }
    }
    fun convertirFloat(num: String) : Float{
        try {
            val numero = num.toFloat()
            return numero
        } catch (e: NumberFormatException) {
            Toast.makeText(this, "Solo admite numero", Toast.LENGTH_SHORT).show()
            return 0.0f
        }
    }
    fun eventosClic(){

        btnSumar.setOnClickListener(View.OnClickListener {
            if(isValido()){
                operaciones = Operaciones(num1, num2)
                txtResultado.text=operaciones.suma().toString()
            }
        })
        btnRestar.setOnClickListener(View.OnClickListener {

            if(isValido()){
                operaciones = Operaciones(num1, num2)
                txtResultado.text=operaciones.resta().toString()
            }
        })
        btnMulti.setOnClickListener(View.OnClickListener {
            if(isValido()){
                operaciones = Operaciones(num1, num2)
                txtResultado.text=operaciones.multiplica().toString()
            }
        })
        btnDiv.setOnClickListener(View.OnClickListener {
            if(isValido()){
                if(num1.equals(0.0f) || num2.equals(0.0f))
                    Toast.makeText(this, "No se puede dividir un 0", Toast.LENGTH_SHORT).show()
                else{
                    operaciones = Operaciones(num1, num2)
                    txtResultado.text=operaciones.dividir().toString()
                }
            }
        })
        btnSalir.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Salir")
                .setMessage("¿Deseas salir al Menu?")

            builder.setPositiveButton("Aceptar") { dialog, which ->
                Toast.makeText(this, "Has salido al menu", Toast.LENGTH_SHORT).show()
                finish()
            }
            builder.setNegativeButton("Cancelar") { dialog, which ->
                Toast.makeText(this, "Has presionado Cancelar.", Toast.LENGTH_SHORT).show()
            }
            val dialog = builder.create()
            dialog.show()
        })
        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtResultado.text=""
            txtNum1.text=Editable.Factory.getInstance().newEditable("")
            txtNum2.text=Editable.Factory.getInstance().newEditable("")
        })
    }
}

