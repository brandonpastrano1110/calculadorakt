package com.example.practicak01

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private lateinit var txtUsuario: EditText;
    private lateinit var txtContrasena: EditText;
    private lateinit var btnIngresar : Button;
    private lateinit var btnSalir : Button;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        eventosClic()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    fun iniciarComponentes(){
        txtUsuario= findViewById(R.id.txtUsuario)
        txtContrasena= findViewById(R.id.txtContrasena)
        btnSalir= findViewById(R.id.btnSalir)
        btnIngresar= findViewById(R.id.btnIngresar)
    }
    fun eventosClic(){
        btnIngresar.setOnClickListener {
            val usuario: String = getString(R.string.usuario)
            val pass: String = getString(R.string.pass)
            val nombre: String = getString(R.string.nombre)

            if (txtUsuario.text.toString() == usuario && txtContrasena.text.toString() == pass) {
                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("nombre", nombre)
                startActivity(intent)
            } else {
                Toast.makeText(this, "El usuario o password no es correcto", Toast.LENGTH_SHORT).show()
            }
        }
        btnSalir.setOnClickListener(View.OnClickListener {
            finish();
        })
    }
}